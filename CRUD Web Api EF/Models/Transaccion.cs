﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace CRUD_Web_Api_EF.Models
{
    public partial class Transaccion
    {
        public int IdTransaccion { get; set; }
        public int IdCuentaBancaria { get; set; }
        public int IdTipoTransaccion { get; set; }
        public decimal Monto { get; set; }

        public virtual CuentaBancaria IdCuentaBancariaNavigation { get; set; }
        public virtual TipoTransaccion IdTipoTransaccionNavigation { get; set; }
    }
}
